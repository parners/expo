module.exports = function(api) {
  api.cache(true);
  return {
      presets: ['babel-preset-expo'],
      plugins: [
        [
          'module-resolver',
          {
            root: ['.'],
            extensions: [
              '.ios.ts',
              '.android.ts',
              '.ts',
              '.ios.tsx',
              '.android.tsx',
              '.tsx',
              '.jsx',
              '.js',
              '.eslintrc.json',
              'd.ts',
              '.json',
            ],
            alias: {
              '@interface': './src/interface/index.ts',
              '@styles': './src/styles/index.ts',
              '@shared': './src/shared/index.ts',
            },
          },
        ],
      ],
  };
};
