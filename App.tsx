import React, {useLayoutEffect, createContext} from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";
import Auth from "./src/module/Auth";
import { ColorInit, FontsInit } from "@styles";
import { useFonts } from "expo-font";
import CoachScreen from "./src/module/Coach";
import AthleteScreen from "./src/module/Athlete";
import {FirebaseProviderApp} from "./src/firebaseApp";

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Home Screen</Text>
    </View>
  );
}
const Stack = createNativeStackNavigator();

export default function App() {
  const [loaded] = useFonts(FontsInit);

  useLayoutEffect(() => {
    ColorInit();
  }, []);

  if (!loaded) {
    return null;
  }
  return (
    <FirebaseProviderApp>
      <SafeAreaProvider>
      <NavigationContainer>
        <SafeAreaView style={{ flex: 1 }}>
          <Stack.Navigator>
          <Stack.Screen
              name="Auth"
              component={Auth}
              options={{ headerShown: false }}
            />
            <Stack.Screen name="Coach" component={CoachScreen}
              options={{ headerShown: false }} />
            <Stack.Screen name="Athlete" component={AthleteScreen}
              options={{ headerShown: false }} />
          </Stack.Navigator>
        </SafeAreaView>
      </NavigationContainer>
    </SafeAreaProvider>
    </FirebaseProviderApp>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
