/* eslint-disable import/prefer-default-export */
/* eslint-disable react/require-default-props */
import {useTheme} from '@react-navigation/native';
import {ColorsInterface} from '@styles';
import React from 'react';
import {ActivityIndicator, View} from 'react-native';

export interface LoadingProps {
  color?: ColorsInterface | string;
  size?: 'smart' | 'medium' | 'large';
}

export const Loading = ({color = '#000', size = 'medium'}: LoadingProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  const _size = () => {
    if (size === 'large') {
      return 100;
    }
    if (size === 'medium') {
      return 80;
    }
    if (size === 'smart') {
      return 50;
    }
    return 50;
  };

  return (
    <View
      style={{
        position: 'absolute',
        zIndex: 4,
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: `${colors.text}60`,
        elevation: 5,
      }}>
      <ActivityIndicator
        size={_size() as number}
        color={color ? String(color) : colors.primary}
      />
    </View>
  );
};
