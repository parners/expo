/* eslint-disable react-hooks/exhaustive-deps */
import {useTheme} from '@react-navigation/native';
import React, {useEffect, useState, useRef} from 'react';
import {
  KeyboardTypeOptions,
  NativeSyntheticEvent,
  StyleProp,
  StyleSheet,
  TextInput,
  TextInputKeyPressEventData,
  TextInputSubmitEditingEventData,
  TextStyle,
  ViewStyle,
} from 'react-native';
import {Input} from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import {ColorsInterface, FontBody} from '@styles';

interface typeInterface {
  autoCompleteType:
    | 'off'
    | 'cc-csc'
    | 'cc-exp'
    | 'cc-exp-month'
    | 'cc-exp-year'
    | 'cc-number'
    | 'email'
    | 'name'
    | 'password'
    | 'postal-code'
    | 'street-address'
    | 'tel'
    | 'username';
  keyboardType: KeyboardTypeOptions;
  textContentType:
    | 'none'
    | 'URL'
    | 'addressCity'
    | 'addressCityAndState'
    | 'addressState'
    | 'countryName'
    | 'creditCardNumber'
    | 'emailAddress'
    | 'familyName'
    | 'fullStreetAddress'
    | 'givenName'
    | 'jobTitle'
    | 'location'
    | 'middleName'
    | 'name'
    | 'namePrefix'
    | 'nameSuffix'
    | 'nickname'
    | 'organizationName'
    | 'postalCode'
    | 'streetAddressLine1'
    | 'streetAddressLine2'
    | 'sublocality'
    | 'telephoneNumber'
    | 'username'
    | 'password'
    | 'newPassword'
    | 'oneTimeCode';
}

export interface InputTextInterface {
  placeholder?: string;
  leftIcon?: string;
  rightIcon?: string;
  label?: string;
  value?: string;
  editable?: boolean;
  secureTextEntry?: boolean;
  type?: 'number' | 'string' | 'email' | 'password' ;
  isError?: boolean;
  inputContainerStyle?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  inputStyle?: StyleProp<ViewStyle>;
  style?: StyleProp<TextStyle>;
  labelStyle?: StyleProp<TextStyle>;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  returnKeyType?:
    | 'done'
    | 'go'
    | 'next'
    | 'search'
    | 'send'
    | 'none'
    | 'previous'
    | 'default'
    | 'emergency-call'
    | 'google'
    | 'join'
    | 'route'
    | 'yahoo';
  onChangeText?: (event: string) => void;
  onError?: (isError: boolean) => void;
  onRightIconPress?: () => void;
  onLefttIconPress?: () => void;
  onKeyPress?: (e: NativeSyntheticEvent<TextInputKeyPressEventData>) => void;
  onSubmitEditing?: (
    e: NativeSyntheticEvent<TextInputSubmitEditingEventData>,
  ) => void;
  onRef?: (current: TextInput) => void;
}

export const InputText = ({
  placeholder = '',
  leftIcon,
  label = '',
  value = '',
  editable = true,
  secureTextEntry = false,
  rightIcon,
  type,
  isError = false,
  inputContainerStyle,
  containerStyle,
  inputStyle,
  labelStyle,
  style,
  returnKeyType,
  autoCapitalize = 'sentences',
  onChangeText,
  onError,
  onRightIconPress,
  onLefttIconPress,
  onKeyPress,
  onSubmitEditing,
  onRef,
}: InputTextInterface) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  const _ref = useRef<TextInput>(null);
  const [stateColor, setStateColor] = useState(`${colors.text}80`);
  const [error, setError] = useState({
    isError: false,
    message: '',
  });

  const [_type, setType] = useState({
    autoCompleteType: 'off',
    keyboardType: 'default',
    textContentType: 'none',
  } as typeInterface);

  useEffect(() => {
    switch (type) {
      case 'email':
        setType({
          autoCompleteType: 'email',
          keyboardType: 'email-address',
          textContentType: 'emailAddress',
        });
        break;
      case 'number':
        setType({
          autoCompleteType: 'tel',
          keyboardType: 'number-pad',
          textContentType: 'telephoneNumber',
        });
        break;

      default:
        setType({
          autoCompleteType: 'off',
          keyboardType: 'default',
          textContentType: 'none',
        });
        break;
    }
  }, []);

  useEffect(() => {
    if (_ref !== null && _ref !== undefined) {
      onRef && onRef(_ref.current as TextInput);
    }
  }, [_ref]);

  useEffect(() => {
    switch (type) {
      case 'email':
        if (value !== '' && value !== null) {
          if (
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
              value,
            )
          ) {
            setError({
              isError: false,
              message: '',
            });
            onError && onError(false);
            setStateColor(`${colors.primary}80`);
          } else {
            onError && onError(true);
            setError({
              isError: true,
              message: 'No es un correo valido',
            });
            setStateColor(colors.danger);
          }
        }
        break;
      case 'password':
        if (value !== '' && value !== null) {
          if (String(value).length <= 7) {
            setError({
              isError: true,
              message: 'Minimo 8 caracteres',
            });
            onError && onError(true);
            setStateColor(colors.danger);
          } else {
            setError({
              isError: false,
              message: '',
            });
            onError && onError(false);
            setStateColor(`${colors.primary}80`);
          }
        }
        break;
      default:
        break;
    }
  }, [value]);

  useEffect(() => {
    if (isError) {
      setError({
        isError: true,
        message: 'Campo requerido',
      });
      onError && onError(isError);
      setStateColor(colors.danger);
    }
    if (editable) {
      setError({
        isError: false,
        message: '',
      });
      onError && onError(isError);
      setStateColor(`${colors.primary}80`);
    } else {
      setError({
        isError: false,
        message: '',
      });
      onError && onError(isError);
      setStateColor(`${colors.card}80`);
    }
  }, [isError === true]);

  const styleInput = StyleSheet.create({
    style: {
      alignItems: 'flex-start',
      color: stateColor,
    },
    containerStyle: {
      alignItems: 'flex-start',
    },
    inputStyle: {
      color: stateColor,
      alignItems: 'center',
      textAlign: 'justify',
      paddingRight: 20,
    },
    leftIconContainerStyle: {
      marginLeft: 0,
    },
    inputContainerStyle: {
      height: 40,
      marginTop: 10,
      textAlign: 'justify',
      borderRadius: 7,
      borderWidth: editable ? 1 : 0,
      borderColor: stateColor,
    },
  });

  return (
    <Input
      placeholder={placeholder}
      autoCompleteType={_type.autoCompleteType}
      keyboardType={_type.keyboardType}
      textContentType={_type.textContentType}
      label={label}
      value={value}
      autoCapitalize={autoCapitalize}
      onKeyPress={onKeyPress}
      onSubmitEditing={onSubmitEditing}
      returnKeyType={returnKeyType}
      errorMessage={error.isError ? error.message : ''}
      onChangeText={onChangeText}
      editable={editable}
      secureTextEntry={secureTextEntry}
      leftIcon={
        <Icons
          name={leftIcon as string}
          color={stateColor}
          size={25}
          style={{marginLeft: 10}}
          onPress={onLefttIconPress}
        />
      }
      rightIcon={
        <Icons
          name={rightIcon as string}
          color={stateColor}
          size={25}
          onPress={onRightIconPress}
        />
      }
      style={[FontBody.Text1, styleInput.style, style]}
      labelStyle={[{color: stateColor}, FontBody.Text1, labelStyle]}
      textAlignVertical="center"
      inputContainerStyle={[
        styleInput.inputContainerStyle,
        inputContainerStyle,
      ]}
      selectionColor={colors.border}
      containerStyle={[styleInput.containerStyle, containerStyle]}
      inputStyle={[styleInput.inputStyle, FontBody.Text1, inputStyle]}
      placeholderTextColor={`${colors.desactive}30`}
      leftIconContainerStyle={styleInput.leftIconContainerStyle}
      errorStyle={{color: colors.danger}}
      ref={_ref}
    />
  );
};
