import React from 'react';
import {
  StyleProp,
  StyleSheet,
  ViewStyle,
  TouchableOpacity,
  Text,
  TextStyle,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {ColorsInterface, FontAction} from '@styles';
import {Icon} from 'react-native-elements';

export interface BtnProps {
  titleStyle?: StyleProp<TextStyle>;
  title: string;
  onPress: () => void;
  disabled?: boolean;
  iconName?: string;
  sizeIcon?: number;
  iconStyle?: StyleProp<TextStyle | ViewStyle | undefined>;
  disabledStyle?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  backgoundcolor?:
    | 'backgroundActionSeconday'
    | 'border'
    | 'card'
    | 'notification'
    | 'primary'
    | 'primaryFaded'
    | 'text'
    | 'subtitle'
    | 'textBgPrimary'
    | 'textBgNotification'
    | 'success'
    | 'successFaded'
    | 'warning'
    | 'warningFaded'
    | 'danger'
    | 'dangerFaded'
    | 'desactive'
    | 'pending'
    | 'muted'
    | 'logout';
  fontColor?:
    | 'backgroundActionSeconday'
    | 'border'
    | 'card'
    | 'notification'
    | 'primary'
    | 'primaryFaded'
    | 'text'
    | 'subtitle'
    | 'textBgPrimary'
    | 'textBgNotification'
    | 'success'
    | 'successFaded'
    | 'warning'
    | 'warningFaded'
    | 'danger'
    | 'dangerFaded'
    | 'desactive'
    | 'pending'
    | 'muted'
    | 'logout';
}

// TODO recuperar botones para ios
export const Btn = ({
  title = 'Texto Boton',
  onPress,
  backgoundcolor = 'primary',
  containerStyle,
  disabled,
  disabledStyle,
  titleStyle,
  iconName,
  fontColor = 'textBgPrimary',
  sizeIcon = 22,
  iconStyle,
}: BtnProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      style={[
        {backgroundColor: disabled ? colors.desactive : colors[backgoundcolor]},
        styles.containerButtonStyle,
        containerStyle,
        disabled ? disabledStyle : containerStyle,
      ]}>
      {iconName ? (
        <Icon
          style={iconStyle}
          name={iconName}
          color={colors[fontColor]}
          size={sizeIcon}
        />
      ) : (
        <></>
      )}
      <Text
        style={[
          FontAction.Text2,
          {textAlign: 'center', color: colors[fontColor]},
          titleStyle,
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerButtonStyle: {
    marginVertical: 10,
    marginHorizontal: 5,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});
