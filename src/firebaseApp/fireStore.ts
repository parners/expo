import FirebaseApp from './index'
import { useContext, useEffect, useReducer, useState } from "react";
import 'firebase/firestore'


export const useGetColection = async (collectionName: string) => {
  const res: { id: any; data: any; }[] = [];
  try {
    const db = FirebaseApp.firestore();
    const querySnapshot = await db.collection(collectionName).get();
    querySnapshot.forEach((doc: { id: any; data: () => any; }) => {
      res.push({
        id: doc.id,
        data: { ...doc.data() },
      })
    });
    return res;
  } catch (error) {
    console.error(error)
    return res
  }
}