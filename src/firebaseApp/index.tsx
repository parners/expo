import _FirebaseApp from 'firebase/app';
import React, { createContext, useEffect, useContext, useReducer, useState } from "react";

export interface FirebaseConfigInterface {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
  measurementId: string;
};

const firebaseConfig:FirebaseConfigInterface = {
  apiKey: "AIzaSyA386d6dIBBazni73Hr9qpR0VeE2ARQKjs",
  authDomain: "parners-40784.firebaseapp.com",
  databaseURL: "https://parners-40784.firebaseio.com",
  projectId: "parners-40784",
  storageBucket: "parners-40784.appspot.com",
  messagingSenderId: "473920114364",
  appId: "1:473920114364:android:d6d24a46dc11d6c9fe8ab9",
  measurementId: "G-KJ25B5GMDT"
};

export const contextApp = createContext <{app: _FirebaseApp.app.App | null; setFirebaseApp: (app: _FirebaseApp.app.App) => void}>({app: null, setFirebaseApp: () => {}});

export const firebaseInit = (firebaseConfig:FirebaseConfigInterface) => {
  try {
    if (!_FirebaseApp.apps.length) {
      return _FirebaseApp.initializeApp(firebaseConfig)
    } else {
      return _FirebaseApp.apps[0];
      }
    } catch (error) {
      if (!/already exist/.test(error.message)) {
        console.error('Firebase initialization error', error.stack);
      }
    return null;
  }
}

export const FirebaseProviderApp = ({ children }) => {
  const [firebaseApp, setFirebaseApp] = useState<_FirebaseApp.app.App | null>(null);

  const saveSettings = (values: _FirebaseApp.app.App) => {
    setFirebaseApp(values)
  };

  useEffect(() => {
    setFirebaseApp(firebaseInit(firebaseConfig));
  }, [])

  return <contextApp.Provider value={{ firebaseApp, saveSettings }} >{children}</contextApp.Provider>;
}

const FirebaseApp = _FirebaseApp

export default FirebaseApp;