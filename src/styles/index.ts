import {StyleProp, ViewStyle} from 'react-native';

export * from './colors';
export * from './fonts';

export const cardStyles: StyleProp<ViewStyle> = {
  marginVertical: 10,
  width: '96%',
  alignSelf: 'center',
  borderRadius: 5,
};
