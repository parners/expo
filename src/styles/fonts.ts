/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {StyleSheet, ViewStyle} from 'react-native';
import fabrigaBlack from './fonts/Fabriga-Black.otf';
import fabrigaBlackItalic from './fonts/Fabriga-BlackItalic.otf';
import fabrigaBold from './fonts/Fabriga-Bold.otf';
import fabrigaBoldItalic from './fonts/Fabriga-BoldItalic.otf';
import fabrigaItalic from './fonts/Fabriga-Italic.otf';
import fabrigaLight from './fonts/Fabriga-Light.otf';
import fabrigaLightItalic from './fonts/Fabriga-LightItalic.otf';
import fabrigaMedium from './fonts/Fabriga-Medium.otf';
import fabrigaMediumItalic from './fonts/Fabriga-MediumItalic.otf';
import fabrigaRegular from './fonts/Fabriga-Regular.otf';

export const FontsInit = {
  'Fabriga-Black': fabrigaBlack,
  'Fabriga-BlackItalic': fabrigaBlackItalic,
  'Fabriga-Bold': fabrigaBold,
  'Fabriga-BoldItalic': fabrigaBoldItalic,
  'Fabriga-Italic': fabrigaItalic,
  'Fabriga-Light': fabrigaLight,
  'Fabriga-LightItalic': fabrigaLightItalic,
  'Fabriga-Medium': fabrigaMedium,
  'Fabriga-MediumItalic': fabrigaMediumItalic,
  'Fabriga-Regular': fabrigaRegular,
};

export const FontTitle = StyleSheet.create({
  h1: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 22,
  } as ViewStyle,
  h2: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 18,
    lineHeight: 20,
  } as ViewStyle,
  h3: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 18,
  } as ViewStyle,
  h4: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 14,
    lineHeight: 16,
  } as ViewStyle,
  h5: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 12,
    lineHeight: 14,
  } as ViewStyle,
  h6: {
    color:'#6D6B70',
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 10,
    lineHeight: 12,
  } as ViewStyle,
});

export const FontBody = StyleSheet.create({
  Text1: {
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'normal',
    fontSize: 18,
  } as ViewStyle,
  Text2: {
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'normal',
    fontSize: 16,
  } as ViewStyle,
  Text3: {
    fontFamily: 'Fabriga-Regular',
    fontWeight: 'normal',
    fontSize: 14,
  } as ViewStyle,
});

export const FontAux = StyleSheet.create({
  Text1: {
    fontFamily: 'Fabriga-Light',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 16,
  } as ViewStyle,
  Text2: {
    fontFamily: 'Fabriga-Light',
    fontWeight: '300',
    fontStyle: 'normal',
    fontSize: 14,
  } as ViewStyle,
  Text3: {
    fontFamily: 'Fabriga-Light',
    fontWeight: '100',
    fontStyle: 'normal',
    fontSize: 12,
  } as ViewStyle,
});

export const FontAction = StyleSheet.create({
  Text1: {
    fontFamily: 'Fabriga-Black',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 18,
  } as ViewStyle,
  Text2: {
    fontFamily: 'Fabriga-Black',
    fontWeight: '300',
    fontStyle: 'normal',
    fontSize: 16,
  } as ViewStyle,
  Text3: {
    fontFamily: 'Fabriga-Black',
    fontWeight: '100',
    fontStyle: 'normal',
    fontSize: 14,
  } as ViewStyle,
});
