import {DefaultTheme, DarkTheme} from '@react-navigation/native';

export interface ColorsInterface {
  background: string;
  backgroundActionSeconday: string;
  border: string;
  card: string;
  notification: string;
  primary: string;
  primaryFaded: string;
  text: string;
  subtitle: string;
  textBgPrimary: string;
  textBgNotification: string;
  success: string;
  successFaded: string;
  warning: string;
  warningFaded: string;
  danger: string;
  dangerFaded: string;
  desactive: string;
  pending: string;
  logout: string;
  muted: string;
  blue: string;
}

export const colorLight: ColorsInterface = {
  background: '#F2F4F5',
  backgroundActionSeconday: '#eef3f7',
  border: '#E3E4E6',
  card: '#FFFFFF',
  notification: '#1a264a',
  primary: '#ff6917',
  primaryFaded: '#eef3f7',
  text: '#5F5F5F',
  textBgPrimary: '#FFFFFF',
  textBgNotification: '#FFFFFF',
  success: '#8bc34a',
  successFaded: '#eafce3',
  warning: '#ffb100',
  warningFaded: '#fff6e0',
  danger: '#ff4c4c',
  dangerFaded: '#ffe6e6',
  desactive: '#727273',
  pending: '#FF7A29',
  subtitle: '#979797',
  logout: '#CB4242',
  muted: '#979899',
  blue: '#134CD8',
};

export const colorDark: ColorsInterface = {
  background: '#F2F4F5',
  backgroundActionSeconday: '#eef3f7',
  border: '#E3E4E6',
  card: '#FFFFFF',
  notification: '#f71963',
  primary: '#134CD8',
  primaryFaded: '#eef3f7',
  text: '#333333',
  textBgPrimary: '#FFFFFF',
  textBgNotification: '#FFFFFF',
  success: '#8bc34a',
  successFaded: '#eafce3',
  warning: '#ffb100',
  warningFaded: '#fff6e0',
  danger: '#ff4c4c',
  dangerFaded: '#ffe6e6',
  desactive: '#727273',
  pending: '#FF7A29',
  subtitle: '#979797',
  logout: '#CB4242',
  muted: '#979899',
  blue: '#134CD8',
};

export const ColorInit = () => {
  DarkTheme.colors = colorDark;
  DefaultTheme.colors = colorLight;
};
