export * from './navigation';


export interface Responsive{
  isError: boolean;
  message: string;
  res: any;
}

export interface LocalitationInterface{
    name: string,
    lon: string,
    lng: string,
}
  
export interface SessionClassInterface{
  date: string,
  assistents: string[],
  urlCalendar: string,
}
export interface ClassGroupInterface {
  id: string;
  name: string;
  localitation: LocalitationInterface,
  nextToClass: string,
  participants: number,
  topParticipants: number,
  classes: SessionClassInterface[],
  students: string [],
    teacher: {
      id: string,
      name: string,
    },
    sport: string,
}

export const GroupClassMock:ClassGroupInterface = {
  id: "12321321321321",
  name: "Basketboll Parque Pio XII IDRD",
  localitation: {
    name: "Parque Pio XII",
    lon: "4.633276",
    lng: "-74.148598",
  },
  nextToClass: "2022-10-01 8 pm",
  participants: 7,
  topParticipants: 23,
  classes: [],
  students: [],
  teacher: {
    id: "1232132132345",
    name: "Julian Rodriguez",
  },
  sport: "Basketball",
}

export const GroupClassEntry:ClassGroupInterface = {
  id: "",
  name: "",
  localitation: {
    name: "",
    lon: "",
    lng: "",
  },
  nextToClass: "",
  participants: 0,
  topParticipants: 1,
  classes: [],
  students: [],
  teacher: {
    id: "",
    name: "",
  },
  sport: "",
}