export interface routeNavigate {
  key: string;
  name: string;
  params: undefined;
  state?: RouterNavigationInterface;
}

export interface RouterNavigationInterface {
  index: number;
  key: string;
  routeNames: string[];
  routes: routeNavigate[];
  stale: boolean;
  type: string;
}


export interface Navigationinterface {
  addListener: (type: string, callback: () => void) => void;
  canGoBack: () => void;
  dangerouslyGetParent?: () => void;
  dangerouslyGetState?: () => void;
  dispatch: (thunk: () => void) => void;
  getParent: () => void;
  getState: () => RouterNavigationInterface;
  goBack: () => void;
  isFocused: () => void;
  jumpTo?: () => void;
  navigate: (name: string, options: any) => void;
  pop: () => void;
  popToTop: () => void;
  push: (name: string, options: any) => void;
  removeListener: (type: any, callback: () => void) => void;
  replace: () => void;
  reset: () => void;
  setOptions: (options: {title: string}) => void;
  setParams: () => void;
}

export interface PropsNavogigateScreen {
  navigation: Navigationinterface;
  route: routeNavigate;
}