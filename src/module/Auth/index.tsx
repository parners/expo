import React, { useState } from "react";
import { Image, KeyboardAvoidingView, StyleSheet, Text, View } from "react-native";
import { Navigationinterface, PropsNavogigateScreen, routeNavigate } from "@interface";
import { useTheme } from "@react-navigation/native";
import { ColorsInterface, FontAux, FontBody } from "@styles";
// eslint-disable-next-line import/extensions
import appJson from "../../../app.json";
import FormLogin from "./FormLogin";
import { Loading } from "@shared";

import logo from '../../assets/logo.png';
export interface AuthScreenProps extends PropsNavogigateScreen  {
}

const AuthScreen = (props: AuthScreenProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  const [resolve, setResolve] = useState({ isError: false, message: "" });
  const [loading, setLoding] = useState(false);
  return (
    <>
      {loading && <Loading size="large" />}
      <View       style={[{ backgroundColor: colors.background }, styles.container]}>      
      <Image source={logo}  style={{height: 80, resizeMode: 'contain'}}  />
      <KeyboardAvoidingView
          behavior="padding"
          style={styles.content}
      >
          <FormLogin
            resultAction={(isError, message) => setResolve({ isError, message })}
            loading={(state) => setLoding(state)} signIn={function (userName: string, password: string): Promise<{ isError: boolean; message: string; }> {
              throw new Error("Function not implemented.");
            } }          />
        <Text
          style={[FontAux.Text2, { color: colors.desactive, marginTop: 20, textAlign:'center' }]}
        >{`V ${appJson.expo.version}`}</Text>
      </KeyboardAvoidingView>
    </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-around",
    alignItems: "center",
    flex: 1,
  },
  content: {
    width: "90%",
    justifyContent: "space-around",
    paddingVertical: 15,
  },
  textCredentials: {
    marginTop: 20,
    fontSize: 16,
  },
});

export default AuthScreen;
