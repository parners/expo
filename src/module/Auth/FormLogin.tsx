/* eslint-disable no-void */
import React, {useEffect, useState} from 'react';
import {TextInput, View} from 'react-native';
import {Btn, InputText} from '@shared';
import {connect} from 'react-redux';
import { useTheme } from '@react-navigation/native';
import { ColorsInterface } from '@styles';

interface FormLoginProps {
  signIn: (
    userName: string,
    password: string,
  ) => Promise<{
    isError: boolean;
    message: string;
  }>;
  resultAction: (isError: boolean, message: string) => void;
  loading: (state: boolean) => void;
}

const FormLogin = (props: FormLoginProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  const [logginInfo, setLogginInfo] = useState({userName: '', password: ''});
  const [isErroruserName, setIsErroruserName] = useState(true);
  const [isErrorPassword, setIsErrorPassword] = useState(true);
  const [btnActive, setBtnActive] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const [refText2, setrefText2] = useState<TextInput>();
  const onSubmit = async () => {
    props.loading && props.loading(true);
    const res = await props.signIn(logginInfo.userName, logginInfo.password);
    props.resultAction && props.resultAction(res.isError, res.message);
    props.loading && props.loading(false);
  };

  useEffect(() => {
    if (logginInfo.userName !== '' && logginInfo.password !== '') {
      if (isErroruserName || isErrorPassword) {
        setBtnActive(true);
      } else {
        setBtnActive(false);
      }
    } else {
      setBtnActive(true);
    }
  }, [isErroruserName, isErrorPassword, logginInfo]);

  const handleKeyPassword = () => {
    if (!btnActive) {
      void onSubmit();
    }
  };

  return (
    <View style={{paddingHorizontal: 15, paddingVertical:15 , backgroundColor: colors.card, elevation: 2 }}>
      <InputText
        autoCapitalize="none"
        placeholder="Usuario"
        type="email"
        onChangeText={event => setLogginInfo({...logginInfo, userName: event})}
        value={logginInfo.userName}
        onError={e => setIsErroruserName(e)}
        rightIcon="person"
        returnKeyType="next"
        onSubmitEditing={() => refText2?.focus()}
      />
      <InputText
        onRef={current => setrefText2(current)}
        placeholder="Contraseña"
        type="password"
        onChangeText={event => setLogginInfo({...logginInfo, password: event})}
        value={logginInfo.password}
        secureTextEntry={!showPassword}
        rightIcon={showPassword ? 'visibility' : 'visibility-off'}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        // TODO solucionar error de tipeo
        onRightIconPress={() => setShowPassword(!showPassword)}
        onError={e => setIsErrorPassword(e)}
        onSubmitEditing={() => handleKeyPassword()}
      />

      <Btn
        title="ENTRAR"
        onPress={() => onSubmit()}
        disabled={btnActive}
        containerStyle={{height: 40}}
        fontColor="textBgPrimary"
      />
    </View>
  );
};


export default FormLogin;
