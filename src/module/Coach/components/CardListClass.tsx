import { useTheme } from "@react-navigation/native";
import { ColorsInterface, FontBody, FontTitle } from "@styles";
import React from "react";
import { ScrollView, Text, View } from "react-native";
import { Octicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';

export interface CardListClassProps {
  id: string;
  name: string;
  locationName: string;
  nextToClass: string;
  participants: number;
  topParticipants: number;
}

const CardListClass = ({
  locationName,
  name,
  nextToClass,
  participants,
  topParticipants,
}: CardListClassProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;
  return (
    <View
      style={{
        borderWidth: 0.3,
        marginVertical: 10,
        padding: 8,
        backgroundColor: colors.card,
        borderRadius: 6,
        elevation: 3,
      }}
    >
      <Text style={[FontTitle.h2, {marginVertical: 7, marginLeft: 18}]}>{name}</Text>
      <View style={{
        flexDirection: "row",
        marginVertical: 4
      }}>
        <Octicons name="location" size={18} color={colors.primary} />
        <Text style={[FontBody.Text1, {marginLeft: 4, textAlignVertical:'center'}]}>{locationName}</Text>
      </View>
      <View style={{
        flexDirection: "row",
        marginVertical: 4
      }}>
        <MaterialCommunityIcons name="update" size={18} color={colors.primary} />
        <Text style={[FontBody.Text1, {marginLeft: 4, textAlignVertical:'center'}]}>{nextToClass}</Text>
      </View>
      <View style={{
        flexDirection: "row",
        marginVertical: 4
      }}>
        <MaterialIcons name="people-alt"  size={18} color={colors.primary}/>
        <Text style={[FontBody.Text1, { marginLeft: 4, textAlignVertical: 'center' }]}>{`${participants} cupo${participants > 1 ? 's' : '' } de ${topParticipants}`}</Text>
      </View>
    </View>
  );
};

export default CardListClass;