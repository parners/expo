import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { View } from 'react-native';
import ScreenHome from './Screen/Home';
import AddGroup from './Screen/AddGroup';

const CoachHome = () => {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator>
{/*        <Stack.Screen name="Home" component={ScreenHome} /> */}
       <Stack.Screen name="AddGroup" component={AddGroup} />
  </Stack.Navigator>
  )
}

export default CoachHome;