import { PropsNavogigateScreen } from "@interface";
import React, {useEffect} from "react";
import { ScrollView, Text, View } from "react-native";
import CardListClass from "../components/CardListClass";
import { AntDesign } from "@expo/vector-icons";
import { useTheme } from "@react-navigation/native";
import { ColorsInterface, FontTitle } from "@styles";
import { useGetColection } from "./../../../firebaseApp/fireStore";

/* import { Firebase } from "firebase"; */

export interface CoachScreenProps extends PropsNavogigateScreen {}

const groups = [
  {
    id: "12321321321321",
    name: "Basketboll Parque Pio XII IDRD",
    localitation: {
      name: "Parque Pio XII",
      lon: "4.633276",
      lng: "-74.148598",
    },
    nextToClass: "2022-10-01 8 pm",
    participants: 7,
    topParticipants: 23,
    classes: [
      {
        date: "2021-06-16T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-18T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-20T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-22T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
    ],
    students: ["12321321321323", "12321321321324", "12321321321325"],
    teacher: {
      id: "1232132132345",
      name: "Julian Rodriguez",
    },
    sport: "Basketball",
  },
  {
    id: "12321321321322",
    name: "Clase de Baile",
    localitation: {
      name: "Parque Pio XII",
      lon: "4.633276",
      lng: "-74.148598",
    },
    nextToClass: "2022-10-01 8 pm",
    participants: 7,
    topParticipants: 23,
    classes: [
      {
        date: "2021-06-16T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-18T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-20T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-22T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
    ],
    students: ["12321321321323", "12321321321324", "12321321321325"],
    teacher: {
      id: "1232132132345",
      name: "Julian Rodriguez",
    },
    sport: "Basketball",
  },
  {
    id: "12321321321323",
    name: "Clase de Baile",
    localitation: {
      name: "Parque Pio XII",
      lon: "4.633276",
      lng: "-74.148598",
    },
    nextToClass: "2022-10-01 8 pm",
    participants: 7,
    topParticipants: 23,
    classes: [
      {
        date: "2021-06-16T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-18T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-20T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
      {
        date: "2021-06-22T08:00:00.000Z",
        assistents: ["12321321321323", "12321321321324"],
        urlCalendar: "url",
      },
    ],
    students: ["12321321321323", "12321321321324", "12321321321325"],
    teacher: {
      id: "1232132132345",
      name: "Julian Rodriguez",
    },
    sport: "Basketball",
  },
];
/* const dbSports = new Firestore('products');
 */
const CoachScreen = (props: CoachScreenProps) => {
  const theme = useTheme();
  const colors: ColorsInterface = theme.colors as ColorsInterface;

/*   const firebase = async () => {
    const res = await useGetColection('deportes')
    console.log(res, 'uuuuuuuu') 
  } */

  useEffect(() => {
   /*  void firebase() */
  }, [])
  return (
    <>
      <View style={{marginHorizontal: 5, marginVertical: 10}}>
        <Text style={[FontTitle.h1]}>Grupos</Text>
      </View>
      <ScrollView style={{ flex: 1, marginTop: 8, marginHorizontal: 10}}>
        {groups.map((item) => (
          <CardListClass
            key={item.id}
            locationName={item.localitation.name}
            name={item.name}
            nextToClass={item.nextToClass}
            id={item.id}
            participants={item.participants}
            topParticipants={item.topParticipants}
          />
        ))}
      </ScrollView>
      <View
        style={{
          backgroundColor: `${colors.primary}`,
          width: 50,
          height: 50,
          elevation: 3,
          borderRadius: 30,
          alignSelf: "flex-end",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: 8,
          borderColor: colors.card,
          borderWidth: 4,
          position: 'absolute',
          bottom: "5%",
          right: "3%"
        }}
      >
        <AntDesign name="addusergroup" size={35} color={colors.notification} />
      </View>
    </>
  );
};

export default CoachScreen;
