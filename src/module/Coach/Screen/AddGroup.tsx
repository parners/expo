import { ClassGroupInterface, GroupClassEntry } from "@interface";
import { Btn, InputText } from "@shared";
import React, { useState } from "react";
import { Text, View } from "react-native";

const AddGroup = (props) => {
  const [group, setGroup] = useState<ClassGroupInterface>(GroupClassEntry);

  const updateGroup = (id:string, value:string) => {
    setGroup({
      ...group,
      [id]: value
    })
  }
  const updateLocalitation = (id:string, value:string) => {
    setGroup({
      ...group,
      localitation: {
        ...group.localitation,
        [id]: value
      }
    })
  }

  return (
    <View>
      <InputText
        autoCapitalize="none"
        placeholder="Parque Simon Bolivar - IDRD"
        type="string"
        label='Nombre'
        onChangeText={(event) =>
          updateGroup('name', event)
        }
        value={group.name}
   /*      onError={(e) => setIsErroruserName(e)}
        rightIcon="person"
        returnKeyType="next"
        onSubmitEditing={() => refText2?.focus()} */
      />
      <InputText
        autoCapitalize="none"
        type='string'
        placeholder="22"
        label='Localización'
        onChangeText={(event) =>
          updateLocalitation('name', event)
        }
        value={String(group.localitation.name)}
      />
      <InputText
        autoCapitalize="none"
        type={'number'}
        placeholder="22"
        label='Participantes Maximos'
        onChangeText={(event) =>
          updateGroup('participants', event)
        }
        value={String(group.participants)}
      />
      <Btn title='agregar' />
    </View>
  );
};

export default AddGroup;
